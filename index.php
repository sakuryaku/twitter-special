<?php

// セッションスタート

session_start();

// OAuth用ライブラリ「twitteroauth」

require_once 'twitteroauth/twitteroauth.php';

// アプリ登録した際に発行された値を入れて下さい。

$consumer_key = '';

$consumer_secret = '';


// call_backする場所

$call_back_url = '';

//--------------------------------------

//セッションのアクセストークンのチェック

//--------------------------------------

if((isset($_SESSION["oauth_token"]) && $_SESSION["oauth_token"] !== NULL) && (isset($_SESSION["oauth_token_secret"]) && $_SESSION["oauth_token_secret"] !== NULL)) {

  // ログインしたらここにくる
header("Location: http://localhost/special/meigen.php");
}

  // ログアウトの状態

  else {

    // オブジェクト生成

    $twitter_oauth_object = new TwitterOAuth (

      $consumer_key,

      $consumer_secret);

    //call_backを指定して request tokenを取得

    $oOauthToken = $twitter_oauth_object->getRequestToken($call_back_url);
	//var_dump($oOauthToken);
    //セッション格納

    $_SESSION['request_token'] = $oOauthToken['oauth_token'];

    $sToken = $oOauthToken['oauth_token'];

    $_SESSION['request_token_secret'] = $oOauthToken['oauth_token_secret'];

    //認証URLの引数 falseの場合はtwitter側で認証確認表示

    if(isset($_GET['authorizeBoolean']) && $_GET['authorizeBoolean'] != '') {

      $bAuthorizeBoolean = false;

    }

      else {

        $bAuthorizeBoolean = true;

      }

    //Authorize url を取得

    $sUrl = $twitter_oauth_object->getAuthorizeURL($sToken, $bAuthorizeBoolean);
	
  }

?>

<!DOCTYPE>
<html lang="ja">
<head>
<meta content="ja" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>twitter special</title>
<style>

body{background-color:black;}

#container{
	width:1000px;
	height:950px;
	margin-right:auto;
	margin-left:auto;
	background-color:white;
}

#header{
	height:200px;
	background-color:blue;
	border-bottom:white 1px solid;
}	

#content{
	width:800px;
	height:700px;
	float:left;
	background-color:pink;
}
#left{
	width:200px;
	height:700px;
	float:left;
	background-color:blue;
}
ul#menu{
		margin-top:0px;
		margin-left:0px;
		margin-left:0px;
		padding-left:0px;
		background-color:red;
}
ul#menu>li{
		list-style-type:none;
}
	
ul#menu>li>a{
		display:block;
		width:200px;
		line-height:60px;
		text-decoration:none;
		text-align:center;
		color:#ffffff;
		background-color:#red;
		border-bottom:solid 1px #ffffff
}
ul#menu>li>a:hover{
		background-color:blue;
}

#center{
	width:600px;
	height:700px;
	float:right;
	background-color:yellow;
}
div#center>p{
	padding-left:30px;
	padding-right:30px;
	line-height:30px;
}
div#center>h1{
	padding-left:30px;
	padding-right:30px;
	line-height:30px;
}
div#center>h2{
	padding-left:30px;
	padding-right:30px;
	line-height:30px;
}
div#center>a{
	padding-left:30px;
	padding-right:30px;
}
#right{
	width:200px;
	height:700px;
	float:right;
	background-color:blue;
}
#footer{
	clear:both;
	height:50px;
	background-color:green;
	text-align:center;
	font-size:28px;
}
</style>
</head>
<body>
<div id="container">
<div id="header"><img src="title.png"alt="twitter special"></div>
<div id="content">
<div id="left">
<ul id="menu">
<li><a href="http://avius.hatenablog.com/"target="_blank">開発者ブログ</a></li>
<li><a href="kisoku.html">利用規則</a></li>
<li><a href="saito.html">このサイトについて</a></li>
<li><a href="kensaku.php">ツイッター検索</a></li>
<li><a>メルマガ登録</a></li>
</ul>
</div>
<div id="center">
<h1>ようこそ！！！　twitter specialへ</h1>
<p>このサイトはtwitter apiを駆使してどこまでできるかに挑戦したサイトです。そしてtwitterの会員サイトにもなっています。
ご自身のtwitter IDでログインできます。内容はツイッター検索機、名言作成ツイッター、片思い解除プログラムとなっております。
興味のある方はさっそくtwitter IDでログインしてください。
</p>
<a href="<?php print($sUrl);?>"><img src="login.png"></a>
<h2>ログインするとできること</h2>
<p>ツイッター検索</p>
<p>名言作成ツイッター</p>
<p>ツイッター片思い解除</p>
<p>これからコンテンツを充実していきたいので期待してください。</p>
</div>
</div>
<div id="right"></div>
<div id="footer">produced by Yasunori Watanabe</div>
</div>
</body>
</html>
