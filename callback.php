<?php
//SESSIONスタート
session_start();
// OAuth用ライブラリ「twitteroauth」
require_once 'twitteroauth/twitteroauth.php';
// アプリ登録した際に発行された値を入れて下さい。
$consumer_key = '';
$consumer_secret = '';
//-------------------------------------
//URLパラメータからoauth_verifierを取得
//-------------------------------------
if(isset($_GET['oauth_verifier']) && $_GET['oauth_verifier'] != '') {
  $sVerifier = $_GET['oauth_verifier'];
	//var_dump($sVerifier);
}
 else {
    echo 'oauth_verifier error!';
    exit;
  }
//-----------------------------------------
//リクエストトークンでOAuthオブジェクト生成
//-----------------------------------------
$oOauth = new TwitterOAuth($consumer_key, $consumer_secret, $_SESSION['request_token'], $_SESSION['request_token_secret']);
//var_dump($oOauth);
//oauth_verifierを使ってAccess tokenを取得
//var_dump($oOauth);
$oAccessToken = $oOauth->getAccessToken($sVerifier);
//var_dump($oAccessToken);
//-------------------------
//取得した値をSESSIONに格納
//-------------------------
$_SESSION['user_id'] = $oAccessToken['user_id'];
$_SESSION['screen_name'] = $oAccessToken['screen_name'];
$_SESSION['oauth_token'] = $oAccessToken['oauth_token'];
$_SESSION['oauth_token_secret'] = $oAccessToken['oauth_token_secret'];
// loginページへリダイレクト
header("Location: http://localhost/special/meigen.php");
?>