<?php
require_once 'twitteroauth/twitteroauth.php';

// アプリ登録した際に発行された値を入れて下さい。
$consumer_key = '';

$consumer_secret = '';

$access_token = '';

$access_token_secret = '';

$to= new TwitterOAuth (

  $consumer_key,

  $consumer_secret,

  $access_token,

  $access_token_secret);

if(isset($_POST["word"])){
$word=htmlspecialchars($_POST["word"]);
$request_method = 'GET';

// オプションで使用する変数
$tw_rest_api = 'https://api.twitter.com/1.1/search/tweets.json';
$option_q =$word;

$option_count = 5;

// オプション指定

$options = array (

  'q' => $option_q, 

  'count' => $option_count);
$tw_obj_request = $to->OAuthRequest (

  $tw_rest_api,

  $request_method,

  $options);

// json形式で取得

$req= json_decode($tw_obj_request, true);
}


?>
<!DOCTYPE>
<html lang="ja">
<head>
<meta content="ja" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>twitter special</title>
<style>

body{background-color:black;}

#container{
	width:1000px;
	height:950px;
	margin-right:auto;
	margin-left:auto;
	background-color:white;
}

#header{
	height:200px;
	background-color:blue;
	border-bottom:white 1px solid;
}	

#content{
	width:800px;
	height:700px;
	float:left;
	background-color:pink;
}
#left{
	width:200px;
	height:700px;
	float:left;
	background-color:blue;
}
ul#menu{
		margin-top:0px;
		margin-left:0px;
		margin-left:0px;
		padding-left:0px;
		background-color:red;
}
ul#menu>li{
		list-style-type:none;
}
	
ul#menu>li>a{
		display:block;
		width:200px;
		line-height:60px;
		text-decoration:none;
		text-align:center;
		color:#ffffff;
		background-color:#red;
		border-bottom:solid 1px #ffffff
}
ul#menu>li>a:hover{
		background-color:blue;
}

#center{
	width:600px;
	height:700px;
	float:right;
	background-color:yellow;
}
div#center>p{
	padding-left:30px;
	padding-right:30px;
	line-height:30px;
}
div#center>h1{
	padding-left:30px;
	padding-right:30px;
	line-height:30px;
}
div#center>h2{
	padding-left:30px;
	padding-right:30px;
	line-height:30px;
}
div#center>a{
	padding-left:30px;
	padding-right:30px;
}
div#center>table{
	align:center;
}
#right{
	width:200px;
	height:700px;
	float:right;
	background-color:blue;
}
#footer{
	clear:both;
	height:50px;
	background-color:green;
	text-align:center;
	font-size:28px;
}
</style>
</head>
<body>
<div id="container">
<div id="header"><img src="title.png"alt="twitter special"></div>
<div id="content">
<div id="left">
<ul id="menu">
<li><a href="http://avius.hatenablog.com/">開発者ブログ</a></li>
<li><a href="kisoku.html">利用規則</a></li>
<li><a href="saito.html">このサイトについて</a></li>
<li><a href="kensaku.php">ツイッター検索</a></li>
<li><a>メルマガ登録</a></li>
</ul>
</div>
<div id="center">
<h1>ようこそ！！！　twitter 検索機へ</h1>
<p>下のテキストボックスにキーワードを入れてください。</p>
<form action="" method="post">
<table align="center">
<tr><td><input type="text"id="word"name="word"></td></tr>
<tr><td><input type="submit"value="検索する"></td></tr>
</table>
</form>
<?php
foreach ($req["statuses"] as $key=>$result) {
  echo $result["user"]["screen_name"] . ": " . $result["text"] . "n";
  echo"<hr>";
}

?>

</div>
</div>
<div id="right"></div>
<div id="footer">produced by Yasunori Watanabe</div>
</div>
</body>
</html>